package InterfaceProb3;

public class Person implements Taxable{
	
	private String name;
	private double income;
	
	public Person(String aName,double anIncome){
		this.name = aName;
		this.income = anIncome;
	}
	
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double Tax;
		if(income>300000){
			Tax = (300000*5/100)+(income - 300000)*10/100;	
		}
		else{
			Tax = income*5/100;
		}
		return Tax;
	}

}
