package InterfaceProb3;

import java.util.ArrayList;

public class TestCaseSumTax {
	
	public TestCaseSumTax(){
		Person ps1 = new Person("Yim",100000);
		Person ps2 = new Person("Keen",500000);
		ArrayList<Taxable> listPs = new ArrayList<Taxable>();
		listPs.add(ps1);
		listPs.add(ps2);
		System.out.println("Tax of Person is "+TaxCalculator.sum(listPs));
		
		Product pd1 = new Product("Computer",20000);
		Product pd2 = new Product("Ram",10000);
		ArrayList<Taxable> listPd = new ArrayList<Taxable>();
		listPd.add(pd1);
		listPd.add(pd2);
		System.out.println("Tax of Product is "+TaxCalculator.sum(listPd));
		
		Company cp1 = new Company("NVIDIA",1000000,500000);
		Company cp2 = new Company("AMD",600000,200000);
		ArrayList<Taxable> listCp = new ArrayList<Taxable>();
		listCp.add(cp1);
		listCp.add(cp2);
		System.out.println("Tax of Company is "+TaxCalculator.sum(listCp));
		
		ArrayList<Taxable> listAll = new ArrayList<Taxable>();
		listAll.add(ps1);
		listAll.add(pd1);
		listAll.add(cp1);
		System.out.println("Tax of All is "+TaxCalculator.sum(listAll));
	}
	
	public static void main(String[] args){
		new TestCaseSumTax();
	}

}
