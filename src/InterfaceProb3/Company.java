package InterfaceProb3;

public class Company implements Taxable{
	
	private String name;
	private double income;
	private double expenses;
	
	public Company(String aName, double anIncome, double anExpenses){
		this.name = aName;
		this.income = anIncome;
		this.expenses = anExpenses;
	}
	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double Tax;
		if(income>expenses){
			Tax = (income - expenses)*30/100;
		}
		else{
			Tax = 0;
		}
		return Tax;
	}

}
