package InterfaceProb3;

public class Product implements Taxable {
	
	private String name;
	private double price;
	
	public Product(String aName, double aPrice){
		this.name = aName;
		this.price = aPrice;
	}

	@Override
	public double getTax() {
		// TODO Auto-generated method stub
		double Tax = price*7/100;
		return Tax;
	}

}
