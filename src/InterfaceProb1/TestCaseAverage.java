package InterfaceProb1;

import InterfaceProb2.Data;
import InterfaceProb2.Measurable;
import InterfaceProb2.Person;

public class TestCaseAverage {
	public TestCaseAverage(){
		Measurable[] listM = new Measurable[3];
		listM[0] = new Person("Mark",170);
		listM[1] = new Person("Yim",180);
		listM[2] = new Person("Black",160);
		System.out.println("Average of Person's height is "+Data.average(listM));
		
	}
	public static void main(String[] args){
		 new TestCaseAverage();
	}
	

}
