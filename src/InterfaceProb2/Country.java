package InterfaceProb2;

public class Country implements Measurable{
	
	private String name;
	private double area;

	public Country(String aName,double anArea){
		this.name = aName;
		this.area = anArea;
	}
	
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return area;
	}
	
	public String toString(){
		return name+"'s area "+area;
		
	}

}
