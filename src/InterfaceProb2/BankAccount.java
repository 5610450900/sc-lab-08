package InterfaceProb2;

public class BankAccount implements Measurable{
	
	private String name;
	private double balance;
	
	public BankAccount(String aName, double aBalance){
		this.name = aName;
		this.balance = aBalance;
	}
	
	public double getMeasure() {
		// TODO Auto-generated method stub
		return balance;
	}
	
	public String toString() {
		return name+"'s balance "+balance;
	}

}
