package InterfaceProb2;

public class TestCaseMin {
	public TestCaseMin(){
		Person p1 = new Person("Mark",170);
		Person p2 = new Person("Yim",160);
		System.out.println(Data.measurableMin(p1,p2));
		
		Country c1 = new Country("Thailand",1000);
		Country c2 = new Country("England",1500);
		System.out.println(Data.measurableMin(c1,c2));
		
		BankAccount b1 = new BankAccount("Boom",500);
		BankAccount b2 = new BankAccount("Boss",600);
		System.out.println(Data.measurableMin(b1,b2));
		
	}
	public static void main(String[] args){
		 new TestCaseMin();
	}

}
