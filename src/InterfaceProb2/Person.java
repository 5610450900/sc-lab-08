package InterfaceProb2;

public class Person implements Measurable{
	private String personName;
	private double height;
	
	public Person(String aPersonName,double aHeight){
		this.personName = aPersonName;
		this.height = aHeight;
	}
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return height;
	}
	
	public String toString() {
		return personName+"'s height "+height;
	}
	

}
